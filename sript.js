function calculate(num1, num2, operator) {
    let result;
  
    switch (operator) {
      case "+":
        result = num1 + num2;
        break;
      case "-":
        result = num1 - num2;
        break;
      case "*":
        result = num1 * num2;
        break;
      case "/":
        result = num1 / num2;
        break;
      default:
        console.log("Некоректна математична операція");
        return; // Вихід з функції в разі некоректної операції
    }
  
    console.log("Результат: " + result);
  }
  
  let num1 = parseFloat(prompt("Введіть перше число:"));
  let num2 = parseFloat(prompt("Введіть друге число:"));
  let operator = prompt("Введіть математичну операцію (+, -, *, /):");
  
  calculate(num1, num2, operator);